public class Lit extends Expression {
    int value;
    public Lit (int v){
        value = v;
    }

    @Override
    public String show() {
        if (value >= 0){
            return " "+value;
        } else {
            return "(" + value + ")";
        }

    }

    @Override
    public int evaluate() {
        return value;
    }
}